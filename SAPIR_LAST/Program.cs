using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SAPIR_LAST;

internal static class Program
{
	[DllImport("SX_WDT.dll")]
	public static extern void ResetWDT0();

	[DllImport("SX_WDT.dll")]
	public static extern int SetWDT0(int nTime, byte nEvent);

	[MTAThread]
	private static void Main()
	{
		Application.Run(new Form1());
	}
}
