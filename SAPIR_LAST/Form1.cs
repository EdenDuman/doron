using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using GPSReader;
using JewishTimes;
using SAPIR_LAST.Properties;

namespace SAPIR_LAST;

public class Form1 : Form
{
	public struct SystemTime
	{
		public ushort Year;

		public ushort Month;

		public ushort DayOfWeek;

		public ushort Day;

		public ushort Hour;

		public ushort Minute;

		public ushort Second;

		public ushort Millisecond;
	}

	private const uint FILE_DEVICE_HAL = 257u;

	private const uint METHOD_BUFFERED = 0u;

	private const uint FILE_ANY_ACCESS = 0u;

	private IContainer components;

	private PictureBox pictureBox1;

	private PictureBox ss_1;

	private PictureBox ss_2;

	private PictureBox mm_1;

	private PictureBox mm_2;

	private PictureBox hh_1;

	private PictureBox hh_2;

	private Label Dot1;

	private Label Dot2;

	private ComboBox comboBox1;

	private Panel panel1;

	private Label LMin;

	private Label LHour;

	private Label LDay;

	private Label LMounth;

	private Label LYear;

	private Label tMin;

	private Label tHour;

	private Label tDay;

	private Label tMounth;

	private Label tYear;

	private Label LSec;

	private Label tSec;

	private PictureBox pictureBox5;

	private PictureBox pictureBox4;

	private PictureBox pictureBox3;

	private PictureBox pictureBox2;

	private Label GPSCount;

	private Label label1;

	private ComboBox comboBox2;

	private string PirkeiAvot = "";

	private DateTime sunTime;

	private bool isGPSportExists;

	private int minutesOfOmer = -1;

	private bool isUserChange;

	private bool isHagOrShabat;

	private bool isNetzSecondsZero;

	private byte MinutesBeforeNetz;

	private int netzS;

	private int netzM;

	private bool IsAllreadyInNetzMode;

	private bool IsAllreadyInOmerMode;

	private int saveYear = 2013;

	private int modChangingTime;

	private bool IsSunRiseFixed;

	private bool IsSunSetFixed;

	private bool mouseLeftHold;

	private bool mouseRightHold;

	private StringFormat stringFormat;

	private IntPtr _hWndTaskBar = FindWindowW("HHTaskBar", null);

	private System.Windows.Forms.Timer timer;

	private System.Windows.Forms.Timer timer60;

	private System.Windows.Forms.Timer timerTimeChange;

	private int counterTimeChanged;

	private DateTime realTime;

	private SystemTime st = default(SystemTime);

	private Brush b = new SolidBrush(Color.Black);

	private Font f = new Font("Thoma", 245f, FontStyle.Bold);

	private Rectangle r = new Rectangle(0, 0, 200, 300);

	private char ss1 = ' ';

	private char ss2 = ' ';

	private char mm1 = ' ';

	private char mm2 = ' ';

	private char hh1 = ' ';

	private char hh2 = ' ';

	private Font f1;

	private Rectangle r1 = new Rectangle(0, 0, 200, 300);

	private JeweishDay jd;

	private byte mode = 1;

	protected override void Dispose(bool disposing)
	{
		if (disposing && components != null)
		{
			components.Dispose();
		}
		Dispose(disposing);
	}

	private void InitializeComponent()
	{
		System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SAPIR_LAST.Form1));
		this.Dot1 = new System.Windows.Forms.Label();
		this.Dot2 = new System.Windows.Forms.Label();
		this.hh_2 = new System.Windows.Forms.PictureBox();
		this.hh_1 = new System.Windows.Forms.PictureBox();
		this.mm_2 = new System.Windows.Forms.PictureBox();
		this.mm_1 = new System.Windows.Forms.PictureBox();
		this.ss_2 = new System.Windows.Forms.PictureBox();
		this.ss_1 = new System.Windows.Forms.PictureBox();
		this.pictureBox1 = new System.Windows.Forms.PictureBox();
		this.comboBox1 = new System.Windows.Forms.ComboBox();
		this.panel1 = new System.Windows.Forms.Panel();
		this.label1 = new System.Windows.Forms.Label();
		this.LMin = new System.Windows.Forms.Label();
		this.LHour = new System.Windows.Forms.Label();
		this.LDay = new System.Windows.Forms.Label();
		this.LMounth = new System.Windows.Forms.Label();
		this.LYear = new System.Windows.Forms.Label();
		this.tMin = new System.Windows.Forms.Label();
		this.tHour = new System.Windows.Forms.Label();
		this.tDay = new System.Windows.Forms.Label();
		this.tMounth = new System.Windows.Forms.Label();
		this.tYear = new System.Windows.Forms.Label();
		this.LSec = new System.Windows.Forms.Label();
		this.tSec = new System.Windows.Forms.Label();
		this.comboBox2 = new System.Windows.Forms.ComboBox();
		this.pictureBox5 = new System.Windows.Forms.PictureBox();
		this.pictureBox4 = new System.Windows.Forms.PictureBox();
		this.pictureBox3 = new System.Windows.Forms.PictureBox();
		this.pictureBox2 = new System.Windows.Forms.PictureBox();
		this.GPSCount = new System.Windows.Forms.Label();
		this.panel1.SuspendLayout();
		base.SuspendLayout();
		this.Dot1.BackColor = System.Drawing.Color.Black;
		this.Dot1.Location = new System.Drawing.Point(627, 325);
		this.Dot1.Name = "Dot1";
		this.Dot1.Size = new System.Drawing.Size(33, 33);
		this.Dot1.Visible = false;
		this.Dot2.BackColor = System.Drawing.Color.Black;
		this.Dot2.Location = new System.Drawing.Point(627, 427);
		this.Dot2.Name = "Dot2";
		this.Dot2.Size = new System.Drawing.Size(33, 33);
		this.Dot2.Visible = false;
		this.hh_2.Image = (System.Drawing.Image)resources.GetObject("hh_2.Image");
		this.hh_2.Location = new System.Drawing.Point(311, 207);
		this.hh_2.Name = "hh_2";
		this.hh_2.Size = new System.Drawing.Size(143, 323);
		this.hh_1.Image = (System.Drawing.Image)resources.GetObject("hh_1.Image");
		this.hh_1.Location = new System.Drawing.Point(456, 207);
		this.hh_1.Name = "hh_1";
		this.hh_1.Size = new System.Drawing.Size(144, 323);
		this.mm_2.Image = (System.Drawing.Image)resources.GetObject("mm_2.Image");
		this.mm_2.Location = new System.Drawing.Point(681, 207);
		this.mm_2.Name = "mm_2";
		this.mm_2.Size = new System.Drawing.Size(143, 323);
		this.mm_1.Image = (System.Drawing.Image)resources.GetObject("mm_1.Image");
		this.mm_1.Location = new System.Drawing.Point(826, 207);
		this.mm_1.Name = "mm_1";
		this.mm_1.Size = new System.Drawing.Size(144, 323);
		this.ss_2.Image = (System.Drawing.Image)resources.GetObject("ss_2.Image");
		this.ss_2.Location = new System.Drawing.Point(601, 361);
		this.ss_2.Name = "ss_2";
		this.ss_2.Size = new System.Drawing.Size(39, 63);
		this.ss_1.Image = (System.Drawing.Image)resources.GetObject("ss_1.Image");
		this.ss_1.Location = new System.Drawing.Point(641, 361);
		this.ss_1.Name = "ss_1";
		this.ss_1.Size = new System.Drawing.Size(39, 63);
		this.pictureBox1.Image = (System.Drawing.Image)resources.GetObject("pictureBox1.Image");
		this.pictureBox1.Location = new System.Drawing.Point(0, 0);
		this.pictureBox1.Name = "pictureBox1";
		this.pictureBox1.Size = new System.Drawing.Size(1280, 720);
		this.pictureBox1.Click += new System.EventHandler(pictureBox1_Click);
		this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(pictureBox1_MouseDown_1);
		this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(pictureBox1_MouseUp);
		this.comboBox1.Location = new System.Drawing.Point(569, 139);
		this.comboBox1.Name = "comboBox1";
		this.comboBox1.Size = new System.Drawing.Size(159, 23);
		this.comboBox1.TabIndex = 11;
		this.comboBox1.Visible = false;
		this.comboBox1.SelectedIndexChanged += new System.EventHandler(comboBox1_SelectedIndexChanged);
		this.panel1.BackColor = System.Drawing.Color.PowderBlue;
		this.panel1.Controls.Add(this.label1);
		this.panel1.Controls.Add(this.LMin);
		this.panel1.Controls.Add(this.LHour);
		this.panel1.Controls.Add(this.LDay);
		this.panel1.Controls.Add(this.LMounth);
		this.panel1.Controls.Add(this.LYear);
		this.panel1.Controls.Add(this.tMin);
		this.panel1.Controls.Add(this.tHour);
		this.panel1.Controls.Add(this.tDay);
		this.panel1.Controls.Add(this.tMounth);
		this.panel1.Controls.Add(this.tYear);
		this.panel1.Controls.Add(this.LSec);
		this.panel1.Controls.Add(this.tSec);
		this.panel1.Location = new System.Drawing.Point(263, 249);
		this.panel1.Name = "panel1";
		this.panel1.Size = new System.Drawing.Size(765, 242);
		this.panel1.Visible = false;
		this.label1.Font = new System.Drawing.Font("Tahoma", 18f, System.Drawing.FontStyle.Bold);
		this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
		this.label1.Location = new System.Drawing.Point(181, 205);
		this.label1.Name = "label1";
		this.label1.Size = new System.Drawing.Size(448, 28);
		this.label1.Text = "יש לכוון את השעון לפי שעון חורף";
		this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
		this.label1.ParentChanged += new System.EventHandler(label1_ParentChanged);
		this.LMin.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.LMin.Location = new System.Drawing.Point(34, 115);
		this.LMin.Name = "LMin";
		this.LMin.Size = new System.Drawing.Size(122, 76);
		this.LMin.Text = "00";
		this.LHour.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.LHour.Location = new System.Drawing.Point(181, 115);
		this.LHour.Name = "LHour";
		this.LHour.Size = new System.Drawing.Size(115, 76);
		this.LHour.Text = "00";
		this.LDay.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.LDay.Location = new System.Drawing.Point(322, 115);
		this.LDay.Name = "LDay";
		this.LDay.Size = new System.Drawing.Size(105, 76);
		this.LDay.Text = "00";
		this.LMounth.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.LMounth.Location = new System.Drawing.Point(457, 115);
		this.LMounth.Name = "LMounth";
		this.LMounth.Size = new System.Drawing.Size(103, 76);
		this.LMounth.Text = "00";
		this.LYear.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.LYear.Location = new System.Drawing.Point(594, 115);
		this.LYear.Name = "LYear";
		this.LYear.Size = new System.Drawing.Size(176, 76);
		this.LYear.Text = "2000";
		this.tMin.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.tMin.Location = new System.Drawing.Point(11, 22);
		this.tMin.Name = "tMin";
		this.tMin.Size = new System.Drawing.Size(148, 76);
		this.tMin.Text = "דקה";
		this.tMin.ParentChanged += new System.EventHandler(tMin_ParentChanged);
		this.tHour.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.tHour.Location = new System.Drawing.Point(165, 22);
		this.tHour.Name = "tHour";
		this.tHour.Size = new System.Drawing.Size(148, 76);
		this.tHour.Text = "שעה";
		this.tDay.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.tDay.Location = new System.Drawing.Point(328, 22);
		this.tDay.Name = "tDay";
		this.tDay.Size = new System.Drawing.Size(99, 76);
		this.tDay.Text = "יום";
		this.tMounth.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.tMounth.Location = new System.Drawing.Point(433, 22);
		this.tMounth.Name = "tMounth";
		this.tMounth.Size = new System.Drawing.Size(166, 76);
		this.tMounth.Text = "חודש";
		this.tYear.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.tYear.Location = new System.Drawing.Point(605, 22);
		this.tYear.Name = "tYear";
		this.tYear.Size = new System.Drawing.Size(145, 76);
		this.tYear.Text = "שנה";
		this.LSec.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.LSec.Location = new System.Drawing.Point(23, 115);
		this.LSec.Name = "LSec";
		this.LSec.Size = new System.Drawing.Size(115, 76);
		this.LSec.Text = "00";
		this.LSec.Visible = false;
		this.tSec.Font = new System.Drawing.Font("Arial", 48f, System.Drawing.FontStyle.Bold);
		this.tSec.Location = new System.Drawing.Point(3, 22);
		this.tSec.Name = "tSec";
		this.tSec.Size = new System.Drawing.Size(166, 76);
		this.tSec.Text = "שניה";
		this.tSec.Visible = false;
		this.comboBox2.Items.Add("אוטומטי");
		this.comboBox2.Items.Add("שעון חורף");
		this.comboBox2.Items.Add("שעון קיץ");
		this.comboBox2.Location = new System.Drawing.Point(459, 139);
		this.comboBox2.Name = "comboBox2";
		this.comboBox2.Size = new System.Drawing.Size(100, 23);
		this.comboBox2.TabIndex = 13;
		this.comboBox2.SelectedIndexChanged += new System.EventHandler(comboBox2_SelectedIndexChanged);
		this.pictureBox5.Image = (System.Drawing.Image)resources.GetObject("pictureBox5.Image");
		this.pictureBox5.Location = new System.Drawing.Point(820, 165);
		this.pictureBox5.Name = "pictureBox5";
		this.pictureBox5.Size = new System.Drawing.Size(143, 175);
		this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
		this.pictureBox5.Visible = false;
		this.pictureBox4.Image = (System.Drawing.Image)resources.GetObject("pictureBox4.Image");
		this.pictureBox4.Location = new System.Drawing.Point(665, 165);
		this.pictureBox4.Name = "pictureBox4";
		this.pictureBox4.Size = new System.Drawing.Size(143, 175);
		this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
		this.pictureBox4.Visible = false;
		this.pictureBox3.Image = (System.Drawing.Image)resources.GetObject("pictureBox3.Image");
		this.pictureBox3.Location = new System.Drawing.Point(473, 165);
		this.pictureBox3.Name = "pictureBox3";
		this.pictureBox3.Size = new System.Drawing.Size(143, 175);
		this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
		this.pictureBox3.Visible = false;
		this.pictureBox2.Image = (System.Drawing.Image)resources.GetObject("pictureBox2.Image");
		this.pictureBox2.Location = new System.Drawing.Point(318, 165);
		this.pictureBox2.Name = "pictureBox2";
		this.pictureBox2.Size = new System.Drawing.Size(143, 175);
		this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
		this.pictureBox2.Visible = false;
		this.GPSCount.BackColor = System.Drawing.Color.Black;
		this.GPSCount.ForeColor = System.Drawing.Color.White;
		this.GPSCount.Location = new System.Drawing.Point(0, 0);
		this.GPSCount.Name = "GPSCount";
		this.GPSCount.Size = new System.Drawing.Size(20, 16);
		this.GPSCount.Text = "00";
		this.GPSCount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
		base.AutoScaleDimensions = new System.Drawing.SizeF(96f, 96f);
		base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
		((System.Windows.Forms.Control)this).ClientSize = new System.Drawing.Size(1280, 720);
		base.ControlBox = false;
		base.Controls.Add(this.comboBox2);
		base.Controls.Add(this.GPSCount);
		base.Controls.Add(this.panel1);
		base.Controls.Add(this.pictureBox5);
		base.Controls.Add(this.pictureBox4);
		base.Controls.Add(this.pictureBox3);
		base.Controls.Add(this.pictureBox2);
		base.Controls.Add(this.comboBox1);
		base.Controls.Add(this.Dot2);
		base.Controls.Add(this.Dot1);
		base.Controls.Add(this.hh_2);
		base.Controls.Add(this.hh_1);
		base.Controls.Add(this.mm_2);
		base.Controls.Add(this.mm_1);
		base.Controls.Add(this.ss_2);
		base.Controls.Add(this.ss_1);
		base.Controls.Add(this.pictureBox1);
		this.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
		base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
		base.MaximizeBox = false;
		base.MinimizeBox = false;
		base.Name = "Form1";
		base.KeyDown += new System.Windows.Forms.KeyEventHandler(Form1_KeyDown);
		this.panel1.ResumeLayout(false);
		base.ResumeLayout(false);
	}

	[DllImport("coredll.dll")]
	private static extern int ShowWindow(IntPtr hwnd, uint command);

	[DllImport("coredll.dll")]
	private static extern IntPtr FindWindowW(string lpClassName, string lpWindowName);

	[DllImport("coredll.dll")]
	public static extern uint SetSystemTime(ref SystemTime lpSystemTime);

	[DllImport("coredll.dll")]
	public static extern void GetSystemTime(ref SystemTime lpSystemTime);

	[DllImport("coredll.dll", SetLastError = true)]
	private static extern bool SetCursorPos(int X, int Y);

	public static Bitmap CopyImage(Bitmap srcBitmap, Rectangle section)
	{
		Bitmap bitmap = new Bitmap(section.Width, section.Height);
		Graphics graphics = Graphics.FromImage(bitmap);
		graphics.DrawImage(srcBitmap, 0, 0, section, GraphicsUnit.Pixel);
		graphics.Dispose();
		return bitmap;
	}

	private void setPictureFromRec(PictureBox p)
	{
		p.Image = CopyImage(Resources._1N, new Rectangle(p.Left, p.Top, p.Width, p.Height));
	}

	private void setCustomIsDaylightSavingTime(string number)
	{
		switch (number)
		{
		case "1":
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom = true;
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd = 1;
			comboBox2.SelectedIndex = 2;
			break;
		case "0":
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom = true;
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd = 0;
			comboBox2.SelectedIndex = 1;
			break;
		default:
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom = false;
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd = 0;
			comboBox2.SelectedIndex = 0;
			break;
		}
	}

	public Form1()
	{
		Program.SetWDT0(256000, 208);
		Program.ResetWDT0();
		SetCursorPos(1279, 719);
		timer60 = new System.Windows.Forms.Timer();
		timer60.Interval = 60000;
		timer60.Tick += Timer_Tick60;
		timer60.Enabled = true;
		timerTimeChange = new System.Windows.Forms.Timer();
		timerTimeChange.Interval = 250;
		timerTimeChange.Tick += Timer_timerTimeChange;
		timerTimeChange.Enabled = false;
		InitializeComponent();
		setPictureFromRec(hh_1);
		setPictureFromRec(hh_2);
		setPictureFromRec(mm_1);
		setPictureFromRec(mm_2);
		setPictureFromRec(ss_1);
		setPictureFromRec(ss_2);
		ss_1.MouseDown += pictureBox1_MouseDown_1;
		ss_2.MouseDown += pictureBox1_MouseDown_1;
		mm_1.MouseDown += pictureBox1_MouseDown_1;
		mm_2.MouseDown += pictureBox1_MouseDown_1;
		hh_1.MouseDown += pictureBox1_MouseDown_1;
		hh_2.MouseDown += pictureBox1_MouseDown_1;
		ss_1.MouseUp += pictureBox1_MouseUp;
		ss_2.MouseUp += pictureBox1_MouseUp;
		mm_1.MouseUp += pictureBox1_MouseUp;
		mm_2.MouseUp += pictureBox1_MouseUp;
		hh_1.MouseUp += pictureBox1_MouseUp;
		hh_2.MouseUp += pictureBox1_MouseUp;
		pictureBox2.Paint += pic2_Paint;
		pictureBox3.Paint += pic3_Paint;
		pictureBox4.Paint += pic4_Paint;
		pictureBox5.Paint += pic5_Paint;
		stringFormat = new StringFormat();
		stringFormat.Alignment = StringAlignment.Center;
		RemoveTaskBar(b: true);
		Place.Initialize();
		StreamReader streamReader = new StreamReader("\\Windows\\PROPERTIES.txt");
		string text = streamReader.ReadToEnd();
		string[] array = text.Split(' ');
		setCustomIsDaylightSavingTime(array[0]);
		((ListControl)comboBox1).DataSource = Place.places.ToList();
		if (string.Compare(array[2], "") != 0)
		{
			comboBox1.SelectedIndex = Convert.ToInt32(array[2]);
		}
		else
		{
			comboBox1.SelectedIndex = 0;
		}
		if (array.Length >= 5)
		{
			try
			{
				if (Convert.ToInt32(array[4]) == 0)
				{
					isNetzSecondsZero = true;
				}
				else
				{
					isNetzSecondsZero = false;
				}
			}
			catch
			{
				isNetzSecondsZero = false;
			}
		}
		if (array[1] == "1")
		{
			IsSunRiseFixed = true;
			IsSunSetFixed = true;
		}
		else if (array[1] == "2")
		{
			IsSunRiseFixed = false;
			IsSunSetFixed = true;
		}
		else if (array[1] == "3")
		{
			IsSunRiseFixed = false;
			IsSunSetFixed = false;
		}
		else
		{
			IsSunRiseFixed = true;
			IsSunSetFixed = true;
		}
		if (array.Length >= 4)
		{
			try
			{
				MinutesBeforeNetz = Convert.ToByte(array[3]);
				if (MinutesBeforeNetz > 60)
				{
					MinutesBeforeNetz = 60;
				}
			}
			catch
			{
				MinutesBeforeNetz = 0;
			}
		}
		if (array.Length >= 5)
		{
			try
			{
				if (Convert.ToInt32(array[4]) == 0)
				{
					isNetzSecondsZero = true;
				}
				else
				{
					isNetzSecondsZero = false;
				}
			}
			catch
			{
				isNetzSecondsZero = false;
			}
		}
		GetSystemTime(ref st);
		realTime = new DateTime(st.Year, st.Month, st.Day, st.Hour, st.Minute, st.Second, st.Millisecond);
		isGPSportExists = GPSClass.TestPort(2);
		if (isGPSportExists)
		{
			GPSClass.InitGpsScan(2, 10);
			GPSClass.OnUpdateStatus += GPSClass_OnUpdateStatus;
			GPSClass.StartGpsScan();
		}
		else if (st.Month == 3 && st.Day >= 2 && st.Month == 3 && st.Day <= 8)
		{
			realTime = realTime.AddHours(1.0);
			st.Year = Convert.ToUInt16(realTime.Year);
			st.Month = Convert.ToUInt16(realTime.Month);
			st.Day = Convert.ToUInt16(realTime.Day);
			st.Hour = Convert.ToUInt16(realTime.Hour);
			st.Minute = Convert.ToUInt16(realTime.Minute);
			st.Second = Convert.ToUInt16(realTime.Second);
			SetSystemTime(ref st);
		}
		jd = new JeweishDay(realTime);
		if (IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom)
		{
			realTime = realTime.AddHours(IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd);
		}
		else if (jd.IsDaylightSavingTime)
		{
			realTime = realTime.AddHours(1.0);
		}
		timer = new System.Windows.Forms.Timer();
		timer.Interval = 1000;
		timer.Tick += Timer_Tick;
		ss_1.Paint += ss_1_Paint;
		ss_2.Paint += ss_2_Paint;
		mm_1.Paint += mm_1_Paint;
		mm_2.Paint += mm_2_Paint;
		hh_1.Paint += hh_1_Paint;
		hh_2.Paint += hh_2_Paint;
		pictureBox1.Paint += pictureBox1_Paint;
		isHagOrShabat = Is_Hag_Or_Shabat(isNext: false);
		sunTime = (IsSunRiseFixed ? jd.FixedSunRise : jd.VisibleSunRise);
		if (isNetzSecondsZero)
		{
			sunTime = JeweishDay.Round(sunTime);
		}
		RefreshPictureAndMode(isInit: true);
		timer.Enabled = true;
		isUserChange = true;
	}

	private void GPSClass_OnUpdateStatus(GPSEventArgs e)
	{
		if (e.Valid && (st.Year != e.dTime.Year || st.Month != e.dTime.Month || st.Day != e.dTime.Day || st.Hour != e.dTime.Hour || st.Minute != e.dTime.Minute))
		{
			st.Year = Convert.ToUInt16(e.dTime.Year);
			st.Month = Convert.ToUInt16(e.dTime.Month);
			st.Day = Convert.ToUInt16(e.dTime.Day);
			st.Hour = Convert.ToUInt16(e.dTime.Hour);
			st.Minute = Convert.ToUInt16(e.dTime.Minute);
			st.Second = Convert.ToUInt16(e.dTime.Second);
			SetSystemTime(ref st);
			GPSCount.BackColor = Color.Green;
			GPSCount.ForeColor = Color.Black;
		}
		else
		{
			GPSCount.BackColor = Color.Black;
			GPSCount.ForeColor = Color.White;
		}
		GPSCount.Text = e.nSattle.ToString();
	}

	private void pic2_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Arial", 170f, FontStyle.Bold);
		r = new Rectangle(0, -45, 143, 323);
		e.Graphics.DrawString((netzM / 10).ToString(), f, b, r, stringFormat);
	}

	private void pic3_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Arial", 170f, FontStyle.Bold);
		r = new Rectangle(0, -45, 143, 323);
		e.Graphics.DrawString((netzM % 10).ToString(), f, b, r, stringFormat);
	}

	private void pic4_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Arial", 170f, FontStyle.Bold);
		r = new Rectangle(0, -45, 143, 323);
		e.Graphics.DrawString((netzS / 10).ToString(), f, b, r, stringFormat);
	}

	private void pic5_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Arial", 170f, FontStyle.Bold);
		r = new Rectangle(0, -45, 143, 323);
		e.Graphics.DrawString((netzS % 10).ToString(), f, b, r, stringFormat);
	}

	public string invertString(string s)
	{
		char[] array = s.ToCharArray();
		Array.Reverse(array);
		return new string(array);
	}

	private void pictureBox1_Paint(object sender, PaintEventArgs e)
	{
		sunTime = (IsSunRiseFixed ? jd.FixedSunRise : jd.VisibleSunRise);
		if (isNetzSecondsZero)
		{
			sunTime = JeweishDay.Round(sunTime);
		}
		if (IsAllreadyInNetzMode)
		{
			StringFormat stringFormat = new StringFormat();
			stringFormat.Alignment = StringAlignment.Center;
			new SolidBrush(Color.Black);
			f1 = new Font("Arial", 130f, FontStyle.Bold);
			r1 = new Rectangle(0, 492, 1280, 300);
			e.Graphics.DrawString(sunTime.ToString("HH:mm:ss"), f1, b, r1, stringFormat);
		}
		else
		{
			if (IsAllreadyInOmerMode)
			{
				return;
			}
			string[] array = jd.HebrewDate.Split(' ');
			Brush brush = new SolidBrush(Color.White);
			f1 = new Font("Arial", 20f, FontStyle.Bold);
			r1 = new Rectangle(310, 0, 700, 30);
			e.Graphics.DrawString("www.meshubatz.co.il        משובץ:  077-320-46-25", f1, brush, r1);
			f1 = new Font("Arial", 15f, FontStyle.Bold);
			r1 = new Rectangle(1200, 0, 71, 27);
			e.Graphics.DrawString("V 6.3", f1, brush, r1);
			f1 = new Font("Arial", 60f, FontStyle.Bold);
			StringFormat stringFormat2 = new StringFormat();
			stringFormat2.Alignment = StringAlignment.Near;
			r1 = new Rectangle(42, 272, 278, 90);
			if (array[2].Length > 2)
			{
				e.Graphics.DrawString(array[1], f1, b, r1, stringFormat2);
			}
			else
			{
				e.Graphics.DrawString(array[1] + " " + invertString(array[2]).TrimEnd('\'').TrimStart('\''), f1, b, r1, stringFormat2);
			}
			stringFormat2.Alignment = StringAlignment.Far;
			r1 = new Rectangle(963, 272, 278, 90);
			if (array[0].Length <= 2)
			{
				e.Graphics.DrawString(invertString(array[0]), f1, b, r1, stringFormat2);
			}
			else
			{
				e.Graphics.DrawString(array[0], f1, b, r1, stringFormat2);
			}
			Rectangle rectangle = new Rectangle(273, 109, 723, 300);
			f1 = new Font("Arial", 45f, FontStyle.Bold);
			string s = jd.ParashaOnly;
			if (jd.ParashaOnly == "שבת חול המועד סוכות")
			{
				s = "שבת סוכות";
			}
			else if (jd.ParashaOnly == "שבת חול המועד פסח")
			{
				s = "שבת פסח";
			}
			e.Graphics.DrawString(s, f1, b, rectangle, this.stringFormat);
			f1 = new Font("Arial", 60f, FontStyle.Bold);
			if (mode == 1)
			{
				r1 = new Rectangle(998, 109, 220, 78);
				e.Graphics.DrawString(jd.AlotHashahar.ToString("HH:mm"), f1, b, r1);
				r1 = new Rectangle(116, 109, 220, 78);
				e.Graphics.DrawString(jd.ZmanTalitVeTfilin.ToString("HH:mm"), f1, b, r1);
				r1 = new Rectangle(483, 604, 350, 78);
				if (IsSunRiseFixed)
				{
					e.Graphics.DrawString(jd.FixedSunRise.ToString("HH:mm:ss"), f1, b, r1);
				}
				else
				{
					e.Graphics.DrawString(jd.VisibleSunRise.ToString("HH:mm:ss"), f1, b, r1);
				}
			}
			else if (mode == 2)
			{
				r1 = new Rectangle(998, 109, 220, 78);
				e.Graphics.DrawString(jd.SofZmanKriatShmaMagen.ToString("HH:mm"), f1, b, r1);
				r1 = new Rectangle(116, 109, 220, 78);
				e.Graphics.DrawString(jd.SofZmanKriatShmaGra.ToString("HH:mm"), f1, b, r1);
				r1 = new Rectangle(116, 603, 220, 78);
				if (PirkeiAvot != "")
				{
					r1 = new Rectangle(116, 603, 320, 78);
					e.Graphics.DrawString(PirkeiAvot, new Font("Arial", 50f, FontStyle.Bold), b, r1);
				}
				else
				{
					e.Graphics.DrawString(jd.SofZmanTfilaGra.ToString("HH:mm"), f1, b, r1);
				}
				r1 = new Rectangle(998, 603, 220, 78);
				e.Graphics.DrawString(jd.SofZmanTfilaMagen.ToString("HH:mm"), f1, b, r1);
			}
			else
			{
				if (mode != 3)
				{
					return;
				}
				r1 = new Rectangle(998, 109, 220, 78);
				e.Graphics.DrawString(jd.MinhaGedola.ToString("HH:mm"), f1, b, r1);
				r1 = new Rectangle(116, 109, 220, 78);
				if (isHagOrShabat)
				{
					e.Graphics.DrawString(jd.MozaeyShabat.ToString("HH:mm"), f1, b, r1);
				}
				else
				{
					e.Graphics.DrawString(jd.ZetHakochavim.ToString("HH:mm"), f1, b, r1);
				}
				r1 = new Rectangle(998, 603, 220, 78);
				if (isHagOrShabat)
				{
					if (IsSunSetFixed)
					{
						e.Graphics.DrawString(jd.FixedSunSet.ToString("HH:mm"), f1, b, r1);
					}
					else
					{
						e.Graphics.DrawString(jd.VisibleSunSet.ToString("HH:mm"), f1, b, r1);
					}
				}
				else
				{
					e.Graphics.DrawString(jd.HadlakatNerotNextShabbat.ToString("HH:mm"), f1, b, r1);
				}
				r1 = new Rectangle(116, 603, 220, 78);
				if (isHagOrShabat)
				{
					e.Graphics.DrawString(jd.RabenuTam.ToString("HH:mm"), f1, b, r1);
				}
				else if (IsSunSetFixed)
				{
					e.Graphics.DrawString(jd.FixedSunSet.ToString("HH:mm"), f1, b, r1);
				}
				else
				{
					e.Graphics.DrawString(jd.VisibleSunSet.ToString("HH:mm"), f1, b, r1);
				}
			}
		}
	}

	private void ss_1_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Thoma", 40f, FontStyle.Bold);
		r = new Rectangle(0, 0, 39, 63);
		e.Graphics.DrawString(ss1.ToString(), f, b, r, stringFormat);
	}

	private void ss_2_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Thoma", 40f, FontStyle.Bold);
		r = new Rectangle(0, 0, 39, 63);
		e.Graphics.DrawString(ss2.ToString(), f, b, r, stringFormat);
	}

	private void mm_1_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Thoma", 180f, FontStyle.Bold);
		r = new Rectangle(0, 37, 143, 323);
		e.Graphics.DrawString(mm1.ToString(), f, b, r, stringFormat);
	}

	private void mm_2_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Thoma", 180f, FontStyle.Bold);
		r = new Rectangle(0, 37, 143, 323);
		e.Graphics.DrawString(mm2.ToString(), f, b, r, stringFormat);
	}

	private void hh_1_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Thoma", 180f, FontStyle.Bold);
		r = new Rectangle(0, 37, 143, 323);
		e.Graphics.DrawString(hh1.ToString(), f, b, r, stringFormat);
	}

	private void hh_2_Paint(object sender, PaintEventArgs e)
	{
		f = new Font("Thoma", 180f, FontStyle.Bold);
		r = new Rectangle(0, 37, 144, 323);
		e.Graphics.DrawString(hh2.ToString(), f, b, r, stringFormat);
	}

	private void Timer_Tick60(object sender, EventArgs e)
	{
		Program.ResetWDT0();
	}

	private void setControlsVisible(bool b)
	{
		mm_1.Visible = b;
		mm_2.Visible = b;
		hh_1.Visible = b;
		hh_2.Visible = b;
		Dot1.Visible = true;
		Dot2.Visible = true;
		pictureBox2.Visible = !b;
		pictureBox3.Visible = !b;
		pictureBox4.Visible = !b;
		pictureBox5.Visible = !b;
	}

	private void Timer_Tick(object sender, EventArgs e)
	{
		if (!timerTimeChange.Enabled)
		{
			GetSystemTime(ref st);
		}
		realTime = new DateTime(st.Year, st.Month, st.Day, st.Hour, st.Minute, st.Second, st.Millisecond);
		if (IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom)
		{
			realTime = realTime.AddHours(IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd);
		}
		else if (jd.IsDaylightSavingTime)
		{
			realTime = realTime.AddHours(1.0);
		}
		sunTime = (IsSunRiseFixed ? jd.FixedSunRise : jd.VisibleSunRise);
		if (isNetzSecondsZero)
		{
			sunTime = JeweishDay.Round(sunTime);
		}
		if (realTime < sunTime && realTime > sunTime.AddMinutes(-MinutesBeforeNetz))
		{
			if (!IsAllreadyInNetzMode)
			{
				ss_1.Visible = false;
				ss_2.Visible = false;
				Dot1.Location = new Point(624, 168);
				Dot2.Location = new Point(624, 287);
				setControlsVisible(b: false);
				IsAllreadyInNetzMode = true;
				pictureBox1.Image = Resources.Netz;
			}
			TimeSpan timeSpan = sunTime - realTime;
			if (timeSpan.Minutes / 10 == 0)
			{
				pictureBox2.Visible = false;
			}
			if (netzM != timeSpan.Minutes)
			{
				if (netzM / 10 != timeSpan.Minutes / 10)
				{
					netzM = timeSpan.Minutes;
					pictureBox2.Invalidate();
					pictureBox3.Invalidate();
				}
				else
				{
					netzM = timeSpan.Minutes;
					pictureBox3.Invalidate();
				}
			}
			if (netzS / 10 != timeSpan.Seconds / 10)
			{
				netzS = timeSpan.Seconds;
				pictureBox4.Invalidate();
				pictureBox5.Invalidate();
			}
			else
			{
				netzS = timeSpan.Seconds;
				pictureBox5.Invalidate();
			}
			return;
		}
		if ((!Is_Hag_Or_Shabat(isNext: false) && realTime.TimeOfDay > jd.ZetHakochavim.AddMinutes(5.0).TimeOfDay && realTime.TimeOfDay < jd.ZetHakochavim.AddMinutes(35.0).TimeOfDay && jd.OmerDescription != "") || (Is_Hag_Or_Shabat(isNext: false) && realTime.TimeOfDay > jd.MotzeyShabat.AddMinutes(5.0).TimeOfDay && realTime.TimeOfDay < jd.MotzeyShabat.AddMinutes(35.0).TimeOfDay && jd.OmerDescription != ""))
		{
			if (!IsAllreadyInOmerMode)
			{
				mm_1.Visible = false;
				mm_2.Visible = false;
				hh_1.Visible = false;
				hh_2.Visible = false;
				ss_1.Visible = false;
				ss_2.Visible = false;
				Dot1.Visible = false;
				Dot2.Visible = false;
				pictureBox2.Visible = false;
				pictureBox3.Visible = false;
				pictureBox4.Visible = false;
				pictureBox5.Visible = false;
				IsAllreadyInOmerMode = true;
			}
			else
			{
				if (minutesOfOmer != realTime.Minute)
				{
					minutesOfOmer = realTime.Minute;
					RefreshOmer();
				}
				DotsOmer(realTime.Second % 3 != 1);
			}
			return;
		}
		if (IsAllreadyInNetzMode)
		{
			Dot1.Location = new Point(624, 326);
			Dot2.Location = new Point(624, 435);
			setControlsVisible(b: true);
			IsAllreadyInNetzMode = false;
			mode = 1;
			pictureBox1.Image = Resources._1N;
		}
		if (IsAllreadyInOmerMode)
		{
			IsAllreadyInOmerMode = false;
			minutesOfOmer = -1;
			pictureBox1.Image = Resources._1N;
			setControlsVisible(b: true);
			mode = 1;
		}
		if (Convert.ToInt32(realTime.ToString("ss")) % 2 == 0)
		{
			Dot1.Visible = false;
			Dot2.Visible = false;
		}
		else
		{
			Dot2.Visible = true;
			Dot1.Visible = true;
		}
		string text = realTime.ToString("HHmmss");
		if ((realTime <= jd.FixedSunRise && realTime >= jd.FixedSunRise.AddMinutes(-5.0) && IsSunRiseFixed) || (realTime <= jd.VisibleSunRise && realTime >= jd.VisibleSunRise.AddMinutes(-5.0) && !IsSunRiseFixed))
		{
			ss_1.Visible = true;
			ss_2.Visible = true;
			ss1 = text[5];
			ss_1.Invalidate();
			if (text[4] - ss2 != 0)
			{
				ss2 = text[4];
				ss_2.Invalidate();
			}
		}
		else
		{
			ss_1.Visible = false;
			ss_2.Visible = false;
		}
		if (text[3] - mm1 != 0)
		{
			mm1 = text[3];
			mm_1.Invalidate();
		}
		if (text[2] - mm2 != 0)
		{
			mm2 = text[2];
			mm_2.Invalidate();
		}
		if (text[1] - hh1 != 0)
		{
			hh1 = text[1];
			hh_1.Invalidate();
		}
		if (text[0] - hh2 != 0)
		{
			hh2 = text[0];
			hh_2.Invalidate();
		}
		RefreshPictureAndMode(isInit: false);
		if (!isGPSportExists && realTime.ToString("HHmmss") == "015959" && realTime.DayOfWeek == DayOfWeek.Sunday && st.Month != 3 && (st.Month != 11 || st.Day != 2))
		{
			Thread.Sleep(60000);
			timer60.Enabled = false;
		}
	}

	private string getPirkeiAvot()
	{
		if (jd.ParashaDetails != "" && realTime.DayOfWeek == DayOfWeek.Saturday)
		{
			if (jd.ParashaDetails.Contains("פרקים "))
			{
				return jd.ParashaDetails.Substring(jd.ParashaDetails.IndexOf("פרקים "));
			}
			if (jd.ParashaDetails.Contains("פרק "))
			{
				return jd.ParashaDetails.Substring(jd.ParashaDetails.IndexOf("פרק "));
			}
		}
		return "";
	}

	private void RefreshPictureAndMode(bool isInit)
	{
		if ((realTime > jd.ZetHakochavim && !isHagOrShabat) || (realTime > jd.MozaeyShabat && isHagOrShabat))
		{
			jd = new JeweishDay(realTime.AddDays(1.0));
			isHagOrShabat = Is_Hag_Or_Shabat(isNext: true);
			mode = 1;
			pictureBox1.Image = Resources._1N;
		}
		else if (realTime > sunTime.AddMinutes(10.0) && mode == 1)
		{
			mode = 2;
			PirkeiAvot = getPirkeiAvot();
			if (PirkeiAvot != "")
			{
				pictureBox1.Image = Resources._2N_avot;
			}
			else
			{
				pictureBox1.Image = Resources._2N;
			}
		}
		else if (realTime > jd.SofZmanTfilaGra && mode == 2)
		{
			mode = 3;
			if (isHagOrShabat)
			{
				pictureBox1.Image = Resources._4N;
			}
			else
			{
				pictureBox1.Image = Resources._3N;
			}
		}
		else if (isInit)
		{
			mode = 1;
			pictureBox1.Image = Resources._1N;
		}
	}

	private void RefreshOmer()
	{
		using Bitmap image = new Bitmap(Resources.Omer);
		using Graphics graphics = Graphics.FromImage(image);
		StringFormat stringFormat = new StringFormat();
		stringFormat.Alignment = StringAlignment.Center;
		new SolidBrush(Color.Black);
		f1 = new Font("Arial", 70f, FontStyle.Bold);
		r1 = new Rectangle(90, 140, 1100, 400);
		graphics.DrawString(jd.OmerDescription, f1, b, r1, stringFormat);
		f1 = new Font("Arial", 150f, FontStyle.Bold);
		r1 = new Rectangle(90, 460, 1100, 600);
		graphics.DrawString(realTime.ToString("HH  mm"), f1, b, r1, stringFormat);
		using Graphics graphics2 = pictureBox1.CreateGraphics();
		graphics2.DrawImage(image, 0, 0);
	}

	private void DotsOmer(bool isNeedDots)
	{
		if (isNeedDots)
		{
			StringFormat stringFormat = new StringFormat();
			stringFormat.Alignment = StringAlignment.Center;
			f1 = new Font("Arial", 150f, FontStyle.Bold);
			r1 = new Rectangle(615, 460, 50, 600);
			using Graphics graphics = pictureBox1.CreateGraphics();
			graphics.DrawString(":", f1, b, r1, stringFormat);
			return;
		}
		r1 = new Rectangle(615, 460, 50, 600);
		using Graphics graphics2 = pictureBox1.CreateGraphics();
		graphics2.DrawImage(Resources.Omer, r1, r1, GraphicsUnit.Pixel);
	}

	private void RemoveTaskBar(bool b)
	{
		if (_hWndTaskBar.ToInt64() != 0 && b)
		{
			ShowWindow(_hWndTaskBar, 0u);
		}
		else
		{
			ShowWindow(_hWndTaskBar, 1u);
		}
	}

	private bool special_night_check()
	{
		string[] array = jd.HebrewDate.Split(' ');
		array.Reverse();
		string strA = array[0] + ' ' + array[1];
		if (string.Compare(strA, "י' תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "כ" + '"' + "א תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "ו' סיוון") == 0)
		{
			return true;
		}
		return false;
	}

	private bool special_day_check()
	{
		string[] array = jd.HebrewDate.Split(' ');
		array.Reverse();
		string strA = array[0] + ' ' + array[1];
		if (string.Compare(strA, "א' תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "ב' תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "י' תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "ט" + '"' + "ו תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "כ" + '"' + "ב תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "ט" + '"' + "ו ניסן") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "כ" + '"' + "א ניסן") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "ו' סיוון") == 0)
		{
			return true;
		}
		return false;
	}

	private bool Is_Hag_Or_Shabat(bool isNext)
	{
		string[] array = jd.HebrewDate.Split(' ');
		array.Reverse();
		string strA = array[0] + ' ' + array[1];
		if (isNext)
		{
			if (realTime.DayOfWeek == DayOfWeek.Thursday)
			{
				return false;
			}
			if (realTime.DayOfWeek == DayOfWeek.Friday)
			{
				return true;
			}
		}
		else
		{
			if (realTime.DayOfWeek == DayOfWeek.Friday)
			{
				return false;
			}
			if (realTime.DayOfWeek == DayOfWeek.Saturday)
			{
				return true;
			}
		}
		if (string.Compare(strA, "ב' תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "י' תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "ט" + '"' + "ו תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "כ" + '"' + "ב תשרי") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "ט" + '"' + "ו ניסן") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "כ" + '"' + "א ניסן") == 0)
		{
			return true;
		}
		if (string.Compare(strA, "ו' סיוון") == 0)
		{
			return true;
		}
		return false;
	}

	private void screen_Handler()
	{
	}

	private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
	{
		JeweishDay.Place = (Place)comboBox1.SelectedValue;
		if (isUserChange)
		{
			try
			{
				string text = "";
				string path = "";
				if (Directory.Exists("\\USBStorage"))
				{
					path = "\\USBStorage\\Telraf\\PROPERTIES.txt";
				}
				else if (Directory.Exists("\\USB Storage"))
				{
					path = "\\USB Storage\\Telraf\\PROPERTIES.txt";
				}
				using (StreamReader streamReader = new StreamReader(path))
				{
					text = streamReader.ReadToEnd();
					string[] array = text.Split(' ');
					array[2] = comboBox1.SelectedIndex.ToString();
					text = array[0];
					for (int i = 1; i < array.Length; i++)
					{
						text = text + " " + array[i];
					}
				}
				using StreamWriter streamWriter = new StreamWriter(path);
				streamWriter.WriteLine(text);
			}
			catch
			{
			}
		}
		comboBox1.Visible = false;
		comboBox2.Visible = false;
		RemoveTaskBar(b: true);
	}

	private void pictureBox1_MouseDown_1(object sender, MouseEventArgs e)
	{
		if (timerTimeChange.Enabled)
		{
			if (e.Button == MouseButtons.Left)
			{
				modChangingTime++;
				modChangingTime %= 5;
				setAllPanelBlack();
				setModeRed(modChangingTime);
			}
			if (e.Button == MouseButtons.Right)
			{
				switch (modChangingTime)
				{
				case 4:
				{
					st.Year++;
					if (saveYear + 15 < st.Year)
					{
						st.Year = (ushort)(saveYear - 15);
					}
					LYear.Text = st.Year.ToString();
					int num = DateTime.DaysInMonth(st.Year, st.Month) + 1;
					st.Day = (ushort)(st.Day % num);
					if (st.Day == 0)
					{
						st.Day = 1;
					}
					LDay.Text = st.Day.ToString();
					break;
				}
				case 3:
				{
					st.Month = (ushort)((st.Month + 1) % 13);
					if (st.Month == 0)
					{
						st.Month = 1;
					}
					LMounth.Text = st.Month.ToString();
					int num = DateTime.DaysInMonth(st.Year, st.Month) + 1;
					st.Day = (ushort)(st.Day % num);
					if (st.Day == 0)
					{
						st.Day = 1;
					}
					LDay.Text = st.Day.ToString();
					break;
				}
				case 2:
				{
					int num = DateTime.DaysInMonth(st.Year, st.Month) + 1;
					st.Day = (ushort)((st.Day + 1) % num);
					if (st.Day == 0)
					{
						st.Day = 1;
					}
					LDay.Text = st.Day.ToString();
					break;
				}
				case 1:
					st.Hour = (ushort)((st.Hour + 1) % 24);
					LHour.Text = st.Hour.ToString();
					break;
				case 0:
					st.Minute = (ushort)((st.Minute + 1) % 60);
					LMin.Text = st.Minute.ToString();
					break;
				case 5:
					st.Second = 0;
					LSec.Text = st.Second.ToString();
					break;
				}
			}
		}
		else if ((PictureBox)sender == pictureBox1)
		{
			if (Control.MousePosition.X < 5 && Control.MousePosition.Y < 5)
			{
				comboBox1.Visible = false;
				comboBox2.Visible = false;
				RemoveTaskBar(b: true);
			}
			else if (Control.MousePosition.X > 1270 && Control.MousePosition.Y < 5)
			{
				RemoveTaskBar(b: false);
				comboBox1.Visible = true;
				comboBox2.Visible = true;
				return;
			}
		}
		if (e.Button == MouseButtons.Right)
		{
			mouseRightHold = true;
			counterTimeChanged = 0;
		}
		if (e.Button == MouseButtons.Left)
		{
			mouseLeftHold = true;
			counterTimeChanged = 0;
		}
		if (mouseRightHold && mouseLeftHold && !timerTimeChange.Enabled)
		{
			modChangingTime = 0;
			LYear.Text = st.Year.ToString();
			LMounth.Text = st.Month.ToString();
			LDay.Text = st.Day.ToString();
			LHour.Text = st.Hour.ToString();
			LMin.Text = st.Minute.ToString();
			LSec.Text = 0.ToString();
			setAllPanelBlack();
			setModeRed(modChangingTime);
			timer.Enabled = false;
			saveYear = st.Year;
			panel1.Visible = true;
			timerTimeChange.Enabled = true;
		}
	}

	private void Form1_KeyDown(object sender, KeyEventArgs e)
	{
		if (e.KeyCode == Keys.F1)
		{
			timer60.Enabled = false;
		}
	}

	private void Timer_timerTimeChange(object sender, EventArgs e)
	{
		counterTimeChanged++;
		if (counterTimeChanged > 40)
		{
			st.Second = 10;
			try
			{
				SetSystemTime(ref st);
			}
			catch
			{
			}
			hh_1.Visible = true;
			hh_2.Visible = true;
			mm_1.Visible = true;
			mm_2.Visible = true;
			Thread.Sleep(200);
			timerTimeChange.Enabled = false;
			realTime = new DateTime(st.Year, st.Month, st.Day, st.Hour, st.Minute, st.Second, st.Millisecond);
			if (IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom)
			{
				realTime = realTime.AddHours(IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd);
			}
			else if (jd.IsDaylightSavingTime)
			{
				realTime = realTime.AddHours(1.0);
			}
			jd = new JeweishDay(realTime);
			isHagOrShabat = Is_Hag_Or_Shabat(isNext: false);
			sunTime = (IsSunRiseFixed ? jd.FixedSunRise : jd.VisibleSunRise);
			if (isNetzSecondsZero)
			{
				sunTime = JeweishDay.Round(sunTime);
			}
			RefreshPictureAndMode(isInit: true);
			panel1.Visible = false;
			timer.Enabled = true;
		}
	}

	private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
	{
		if (e.Button == MouseButtons.Right)
		{
			mouseRightHold = false;
		}
		if (e.Button == MouseButtons.Left)
		{
			mouseLeftHold = false;
		}
	}

	private void setAllPanelBlack()
	{
		tYear.ForeColor = Color.Black;
		tMounth.ForeColor = Color.Black;
		tDay.ForeColor = Color.Black;
		tHour.ForeColor = Color.Black;
		tMin.ForeColor = Color.Black;
		tSec.ForeColor = Color.Black;
		LYear.ForeColor = Color.Black;
		LMounth.ForeColor = Color.Black;
		LDay.ForeColor = Color.Black;
		LHour.ForeColor = Color.Black;
		LMin.ForeColor = Color.Black;
		LSec.ForeColor = Color.Black;
	}

	private void setModeRed(int n)
	{
		switch (n)
		{
		case 4:
			tYear.ForeColor = Color.Red;
			LYear.ForeColor = Color.Red;
			break;
		case 3:
			tMounth.ForeColor = Color.Red;
			LMounth.ForeColor = Color.Red;
			break;
		case 2:
			tDay.ForeColor = Color.Red;
			LDay.ForeColor = Color.Red;
			break;
		case 1:
			tHour.ForeColor = Color.Red;
			LHour.ForeColor = Color.Red;
			break;
		case 0:
			tMin.ForeColor = Color.Red;
			LMin.ForeColor = Color.Red;
			break;
		case 5:
			tSec.ForeColor = Color.Red;
			LSec.ForeColor = Color.Red;
			break;
		}
	}

	private void pictureBox1_Click(object sender, EventArgs e)
	{
	}

	private void tMin_ParentChanged(object sender, EventArgs e)
	{
	}

	private void label1_ParentChanged(object sender, EventArgs e)
	{
	}

	private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
	{
		string text = "N";
		switch (comboBox2.SelectedIndex)
		{
		case 1:
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom = true;
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd = 0;
			text = "0";
			break;
		case 2:
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom = true;
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd = 1;
			text = "1";
			break;
		default:
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustom = false;
			IsDaylightSavingTimeCustomParam.IsDaylightSavingTimeCustomNumberToAdd = 0;
			text = "N";
			break;
		}
		if (isUserChange)
		{
			try
			{
				string text2 = "";
				string path = "";
				if (Directory.Exists("\\USBStorage"))
				{
					path = "\\USBStorage\\Telraf\\PROPERTIES.txt";
				}
				else if (Directory.Exists("\\USB Storage"))
				{
					path = "\\USB Storage\\Telraf\\PROPERTIES.txt";
				}
				using (StreamReader streamReader = new StreamReader(path))
				{
					text2 = streamReader.ReadToEnd();
					string[] array = text2.Split(' ');
					array[0] = text;
					text2 = array[0];
					for (int i = 1; i < array.Length; i++)
					{
						text2 = text2 + " " + array[i];
					}
				}
				using StreamWriter streamWriter = new StreamWriter(path);
				streamWriter.WriteLine(text2);
			}
			catch
			{
			}
		}
		comboBox1.Visible = false;
		comboBox2.Visible = false;
		RemoveTaskBar(b: true);
	}
}
