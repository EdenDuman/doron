using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;

namespace SAPIR_LAST.Properties;

[DebuggerNonUserCode]
internal class Resources
{
	private static ResourceManager resourceMan;

	private static CultureInfo resourceCulture;

	[EditorBrowsable(EditorBrowsableState.Advanced)]
	internal static ResourceManager ResourceManager
	{
		get
		{
			if (object.ReferenceEquals(resourceMan, null))
			{
				ResourceManager resourceManager = new ResourceManager("SAPIR_LAST.Properties.Resources", typeof(Resources).Assembly);
				resourceMan = resourceManager;
			}
			return resourceMan;
		}
	}

	[EditorBrowsable(EditorBrowsableState.Advanced)]
	internal static CultureInfo Culture
	{
		get
		{
			return resourceCulture;
		}
		set
		{
			resourceCulture = value;
		}
	}

	internal static Bitmap _1N
	{
		get
		{
			object @object = ResourceManager.GetObject("_1N", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap _2N
	{
		get
		{
			object @object = ResourceManager.GetObject("_2N", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap _2N_avot
	{
		get
		{
			object @object = ResourceManager.GetObject("_2N_avot", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap _3N
	{
		get
		{
			object @object = ResourceManager.GetObject("_3N", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap _4N
	{
		get
		{
			object @object = ResourceManager.GetObject("_4N", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap HH1
	{
		get
		{
			object @object = ResourceManager.GetObject("HH1", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap HH2
	{
		get
		{
			object @object = ResourceManager.GetObject("HH2", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static byte[] JewishTimes
	{
		get
		{
			object @object = ResourceManager.GetObject("JewishTimes", resourceCulture);
			return (byte[])@object;
		}
	}

	internal static Bitmap mm1
	{
		get
		{
			object @object = ResourceManager.GetObject("mm1", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap mm2
	{
		get
		{
			object @object = ResourceManager.GetObject("mm2", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap Netz
	{
		get
		{
			object @object = ResourceManager.GetObject("Netz", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap Omer
	{
		get
		{
			object @object = ResourceManager.GetObject("Omer", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap ss1
	{
		get
		{
			object @object = ResourceManager.GetObject("ss1", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal static Bitmap ss2
	{
		get
		{
			object @object = ResourceManager.GetObject("ss2", resourceCulture);
			return (Bitmap)@object;
		}
	}

	internal Resources()
	{
	}
}
